# TODO: shouldn't need to import gameobjects
from ebigo import gameobjects

import boomObjects

class Game(gameobjects.PlayObject):
    def __init__(self, windowwidth, windowheight):
        super(Game, self).__init__(windowwidth, windowheight)
        playfield = self.addFieldObject(boomObjects.Arena())
        character = self.addObject(boomObjects.BoomGuy(320, 240))
        missile = self.addObject(
            gameobjects.Missile(0, 0, 8, 8),
            playfield
        )
        missile.name = 'bullet'
        character.addMissile(missile)
        self.PLAYGROUND = 1  # The layer of boxmaps that contains obstacles
        self.PLATFORMS = 2

    def playGame(self, controldicts):
        super(Game, self).playGame(controldicts)
        # Get the first player object
        object = self.allObjs[0]
        # Get the playing field
        field = self.getField()
        # collide with floor or start falling
        boxValue = 0
        for i in range(len(field.boxmaps)):
            if i > 0:
                boxRect, val = field.getBoxAtRect(object, self.DOWN, i)
                if val != 0:
                    boxValue = val
        object.gotoGround(boxRect, boxValue)
        # Collide with walls
        if object.dx != 0:
            if object.dx < 0:
                direction = self.LEFT
            elif object.dx > 0:
                direction = self.RIGHT
            boxRect, boxValue = field.getBoxAtRect(
                object, direction, self.PLAYGROUND
#                object, direction
            )
            if boxValue != 0:
                object.moveOutside(
                    boxRect, object.DIRECT2OPPOSITE[direction]
                )
                object.dx = 0

