from ebigo import metagame

import boomGame

class Meta(metagame.MetaGame):
    def __init__(self, winwidth, winheight):
        game = boomGame.Game(winwidth, winheight)
        super(Meta, self).__init__(game, winwidth, winheight)

    def getSpriteSets(self):
        return [
            {'name': "Cyan", 'width': 50, 'height': 50,
             'alignment': 'bottom', 'steps': 1,
             'flips': (360,)},
            {'name': "Cyan_jump", 'width': 50, 'height': 50,
             'alignment': 'bottom', 'steps': 1,
             'flips': (360,)},
            {'name': "Cyan_duck", 'width': 50, 'height': 50,
             'alignment': 'bottom', 'steps': 1,
             'flips': (360,)},
            {'name': "Cyan_run", 'width': 50, 'height': 50,
             'alignment': 'bottom', 'steps': 2,
             'flips': (360,)},
            {'name': "Cyan_rungun", 'width': 50, 'height': 57,
             'alignment': 'bottom', 'steps': 2,
             'directions': (210,150,225,135),
             'flips': (330,30,315,45)},
            {'name': "Cyan_standgun", 'width': 50, 'height': 57,
             'alignment': 'bottom', 'steps': 2,
             'directions': (210,225,270,150,135,90),
             'flips': (330,315,271,30,45,89)
            },
            {'name': "Cyan_damage", 'width': 50, 'height': 50,
             'alignment': 'bottom', 'steps': 2,
             'flips': (360,)},
            {'name': "bullet", 'width': 15, 'height': 15,
             'colorkey': (0,0,0), 'steps': 1,
             'directions': (180,150,135),
             'rots':       (270,240,225),
             'durots':     (360,330,315),
             'trirots':    (90,60,45),
            }
        ]
