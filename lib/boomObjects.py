from ebigo import gameobjects

class BoomGuy(gameobjects.Shooter):
    def __init__(self, x, y):
        self.NAMES = (
            "Cyan", "Violet", "Sky", "Crimson",
            "Pit", "Stark", "Jade", "Amber",
            "Plum", "Grey", "Crystal", "Ivy",
            "Ruby", "Scarlet"
        )
        name = self.NAMES[0]
        width = 30
        height = 50
        super(BoomGuy, self).__init__(name, x, y, width, height)
        self.setAnchor(self.MIDBOTTOM)
        self.JUMP = 'jump'
        self.jumpTimer = 0
        self.LEFTOFUP = self.UP+1
        self.LEFTOFDOWN = self.DOWN-1
        self.RIGHTOFDOWN = self.DOWN+1
        self.RIGHTOFUP = self.UP-1
        self.ANYLEFTFACE = (self.LEFT, self.DOWNLEFT, self.UPLEFT,
            self.LEFTOFUP, self.LEFTOFDOWN)
        self.ANYRIGHTFACE = (self.RIGHT, self.DOWNRIGHT, self.UPRIGHT,
            self.RIGHTOFUP, self.RIGHTOFDOWN)

    def gotoGround(self, boxrect, boxvalue):
        if boxvalue != 0 and self.dy > -1:
            self.moveOutside(boxrect, self.UP)
            self.dy = 0
            if self.status == self.JUMP:
                self.status = None
        else:
            self.status = self.JUMP
            if self.dy < 18:
                self.dy += 1

    def doDirection(self, direction, speed):
        topspeed = 4
        accel = 1
        # perform an action depending on the direction of controls
        if self.status == self.JUMP:
            if direction in self.ANYLEFT:
                if self.dx > -topspeed:
                    self.dx -= accel
                self.direction = self.LEFT
            elif direction in self.ANYRIGHT:
                if self.dx < topspeed:
                    self.dx += accel
                self.direction = self.RIGHT
        else:
            # any left or right direction causes movement
            if direction in self.ANYLEFT:
                if self.dx > -topspeed:
                    self.dx -= accel
                self.direction = self.LEFT
            elif direction in self.ANYRIGHT:
                if self.dx < topspeed:
                    self.dx += accel
                self.direction = self.RIGHT
            else:
                self.dx = 0
            if direction in (self.LEFT, self.RIGHT):
                    self.status = 'run'
                    self.takeStep()
            elif direction in self.ANYUP:
                self.dy = -10
                self.status = self.JUMP
            elif direction in (self.DOWN,):
                self.status = 'duck'
            else:
                self.status = None

    def doAction(self, index):
        self.fireMissile()

    def getFirePos(self, direction):
        if direction == self.RIGHT:
            return self.centerx+15, self.centery-7
        else:
            return self.centerx-16, self.centery-7

    def fireMissile(self):
        fireX, fireY = self.getFirePos(self.direction)
        self.missile.fire(fireX, fireY, self.direction, 12)


class Arena(gameobjects.FieldObject):
    def __init__(self):
        name = "Arena 1"
        x = -25
        y = -25
        width = 850
        height = 650
        mapwidth = 17
        super(Arena, self).__init__(x, y, width, height)
        backgroundmap = self.generateBackground()
        self.addBoxMap('background', backgroundmap, 50, 50, 17)
        playgroundmap = [
            32,32,32,32,32,32,33, 0, 0, 0,31,32,32,32,32,32,32,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
             9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7,
             2, 2, 2, 2, 2, 2, 3, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2
        ]
        self.addBoxMap('playground', playgroundmap, 50, 50, 17)
        platformmap = [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0,
            0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ]
        self.addBoxMap('platforms', platformmap, 50, 50, 17)

    def generateBackground(self):
        map = [
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
            1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        ]
        for y in range(len(map)):
#            for x in range(len(map[y])):
                decorNum = 10
                if self.getChance(24, 1):
                    decorNum += self.getNumber(9)
#                else:
#                    decorNum += 1
#                map[y][x] += decorNum
#                if map[y][x] == 6:
#                    map[y][x] = 1
                map[y] += decorNum
                if map[y] == 6:
                    map[y] = 1
        return map
